
<?php 

// Change this to your connection info.
$DATABASE_HOST = 'YourHost';
$DATABASE_USER = 'YourDatabaseUser';
$DATABASE_PASS = 'YourDatabasePassword';
$DATABASE_NAME = 'YourDatabaseName';


// Try and connect using the info above.
$conn = new mysqli($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


//echo ' submitting...';

$name = $_POST['g_name'];
$message = $_POST['g_message'];

//this is commented out but might be useful for debugging. NOTE: there are no special chars so javascript can be executed here
//but it would only hurt the attacker 
/*
echo $name;
echo $message;
*/

//SQL query
$stmt = $conn->prepare("INSERT INTO `guestbook` (g_name, g_message) VALUES (?, ?)");

//echo 'statement prepared!';

if ($stmt){
//Prepared statement to secure from SQL injection
$stmt->bind_param("ss", $name, $message);

$stmt->execute();

//echo' and done!';



//to redirect after the query to your site, enter the url you want to go to
header("Location: http://www.yourwebsite.com/");




}
else{
    echo 'something went wrong!';
}

?>




