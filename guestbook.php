<html>
	<head>
		<meta charset="utf-8">
		<title>Guestbook</title>
	</head>
	<body>
		<div class="input_guest">
			<h1>Guestbook</h1>

            <!--here in you submit info-->
			<form action="submitguest.php" method="post" autocomplete="off">
				<input type="g_name" name="g_name" placeholder="your name" id="g_name" required>
                <input type="g_message" name="g_message" placeholder="your message" id="g_message" required>
				<input type="submit" value="submit_button">
			</form>
        </div>
            <!--here are the previous guests-->
            <div class="input_guest">
            <?php 

            //database info
            // Change this to your connection info.
            $DATABASE_HOST = 'YourHost';
            $DATABASE_USER = 'YourDatabaseUser';
            $DATABASE_PASS = 'YourDatabasePassword';
            $DATABASE_NAME = 'YourDatabaseName';

            // Create connection
            $conn = new mysqli($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT g_name, g_message FROM guestbook";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
            // output data of each row
            
            while($row = $result->fetch_assoc()) {
                echo'Name: '.htmlspecialchars($row["g_name"]);
                echo'<br>';
                echo'Message: '.htmlspecialchars($row["g_message"]);
                echo'<br>';
                echo'_______';
                echo'<br>';

            }
            
            } else {
                echo "<p>Be the first to write something here!</p>";
            }
            $conn->close();    
            
            ?>
            </div>

	</body>
</html>

