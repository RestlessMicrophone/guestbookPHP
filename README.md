# GuestbookPHP

A small guestbook written in PHP for personal sites and a present for a lainon.

# Instructions

simply put the files on a server running PHP.

guestbook.php contains the form for guestbook and also a query to show the previously visited guests. Uses specialhtmlchars to be secure from javascript in the input.

submitrequest.php is where the form redirects to after an action is taken. There it makes a prepared statement to be secure from SQL injection.

tablequery.txt contains the Query to put in mysql to get the table created


# important!

all echo statements in submitrequest.php are commented out. Feel free to use them though in case your code breaks on certain steps